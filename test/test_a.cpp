#define CATCH_CONFIG_MAIN  // inserisce il main(). Da usare solo in un file .cpp
                           // per ogni eseguibile
#include "catch.hpp"

#include "../src/a.h"

TEST_CASE("somma", "[]") {
  REQUIRE( somma(1,2,3)== 6);
  REQUIRE( somma(0,0,0)== 0);
  REQUIRE( somma(1,2,-3)== 0);
  REQUIRE( somma(-1,-2,-3)== -6);
}
